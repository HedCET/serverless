const Method = method => req =>
  req.method.toLowerCase() === method.toLowerCase();

const Delete = Method("delete");
const Get = Method("get");
const Head = Method("patch");
const Options = Method("options");
const Patch = Method("patch");
const Post = Method("post");
const Put = Method("put");

const Header = (key, value) => req => req.headers.get(key) === value;

const Host = host => Header("host", host.toLowerCase());
const Referrer = host => Header("referrer", host.toLowerCase());

const Path = regExp => req => {
  const path = new URL(req.url).pathname;

  return path.match(regExp) && path.match(regExp)[0] === path;
};

class Router {
  constructor() {
    this.routes = [];
  }

  route(conditions, handler) {
    this.routes.push({
      conditions,
      handler
    });

    return this;
  }

  all(handler) {
    return this.route([], handler);
  }

  get(url, handler) {
    return this.route([Get, Path(url)], handler);
  }

  post(url, handler) {
    return this.route([Post, Path(url)], handler);
  }

  resolve(req) {
    return this.routes.find(route => {
      if (
        !route.conditions ||
        (Array.isArray(route) && !route.conditions.length)
      ) {
        return true;
      }

      if (typeof route.conditions === "function") {
        return route.conditions(req);
      }

      return route.conditions.every(condition => condition(req));
    });
  }

  handle(req) {
    const route = this.resolve(req);

    if (route) {
      return route.handler(req);
    }

    return new Response("resource not found", {
      headers: {
        "content-type": "text/plain"
      },
      status: 404,
      statusText: "not found"
    });
  }
}

module.exports = Router;
