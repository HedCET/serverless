const Router = require("./router");

/**
 * fetch event listener.
 */
addEventListener("fetch", event => {
  event.respondWith(handleRequest(event.request));
});

function customResponse(req) {
  return new Response(JSON.stringify(req), {
    headers: { "content-type": "application/json" }
  });
}

/**
 * request handler for fetch event.
 * @param {Request} req
 */
async function handleRequest(req) {
  const myRouter = new Router();

  myRouter.get(".*/status", () => new Response("/status", { status: 200 }));

  myRouter.get(".*/get.*", req => customResponse(req));
  myRouter.post(".*/post.*", req => customResponse(req));

  myRouter.get(".*", () => new Response("success", { status: 200 }));

  const res = await myRouter.handle(req);
  return res;
}
