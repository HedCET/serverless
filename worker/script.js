const window = this;
!(function(t) {
  var e = {};
  function n(o) {
    if (e[o]) return e[o].exports;
    var r = (e[o] = { i: o, l: !1, exports: {} });
    return t[o].call(r.exports, r, r.exports, n), (r.l = !0), r.exports;
  }
  (n.m = t),
    (n.c = e),
    (n.d = function(t, e, o) {
      n.o(t, e) || Object.defineProperty(t, e, { enumerable: !0, get: o });
    }),
    (n.r = function(t) {
      "undefined" != typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(t, Symbol.toStringTag, { value: "Module" }),
        Object.defineProperty(t, "__esModule", { value: !0 });
    }),
    (n.t = function(t, e) {
      if ((1 & e && (t = n(t)), 8 & e)) return t;
      if (4 & e && "object" == typeof t && t && t.__esModule) return t;
      var o = Object.create(null);
      if (
        (n.r(o),
        Object.defineProperty(o, "default", { enumerable: !0, value: t }),
        2 & e && "string" != typeof t)
      )
        for (var r in t)
          n.d(
            o,
            r,
            function(e) {
              return t[e];
            }.bind(null, r)
          );
      return o;
    }),
    (n.n = function(t) {
      var e =
        t && t.__esModule
          ? function() {
              return t.default;
            }
          : function() {
              return t;
            };
      return n.d(e, "a", e), e;
    }),
    (n.o = function(t, e) {
      return Object.prototype.hasOwnProperty.call(t, e);
    }),
    (n.p = ""),
    n((n.s = 0));
})([
  function(t, e, n) {
    const o = n(1);
    function r(t) {
      return new Response(JSON.stringify(t), {
        headers: { "content-type": "application/json" }
      });
    }
    addEventListener("fetch", t => {
      t.respondWith(
        (async function(t) {
          const e = new o();
          return (
            e.get(".*/status", () => new Response("/status", { status: 200 })),
            e.get(".*/get.*", t => r(t)),
            e.post(".*/post.*", t => r(t)),
            e.get(".*", () => new Response("success", { status: 200 })),
            await e.handle(t)
          );
        })(t.request)
      );
    });
  },
  function(t, e) {
    const n = t => e => e.method.toLowerCase() === t.toLowerCase(),
      o = (n("delete"), n("get")),
      r = (n("patch"), n("options"), n("patch"), n("post")),
      s = (n("put"),
      t => e => {
        const n = new URL(e.url).pathname;
        return n.match(t) && n.match(t)[0] === n;
      });
    t.exports = class {
      constructor() {
        this.routes = [];
      }
      route(t, e) {
        return this.routes.push({ conditions: t, handler: e }), this;
      }
      all(t) {
        return this.route([], t);
      }
      get(t, e) {
        return this.route([o, s(t)], e);
      }
      post(t, e) {
        return this.route([r, s(t)], e);
      }
      resolve(t) {
        return this.routes.find(
          e =>
            !(e.conditions && (!Array.isArray(e) || e.conditions.length)) ||
            ("function" == typeof e.conditions
              ? e.conditions(t)
              : e.conditions.every(e => e(t)))
        );
      }
      handle(t) {
        const e = this.resolve(t);
        return e
          ? e.handler(t)
          : new Response("resource not found", {
              headers: { "content-type": "text/plain" },
              status: 404,
              statusText: "not found"
            });
      }
    };
  }
]);
